/* eslint-disable prettier/prettier */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        animals: []
    },
    mutations: {
        openCreateModal(state, payload) {
            state.open = payload
        },
        createNewAnimal(state, payload) {
            state.animals.push(payload)
        }
        // openEditModel(state, load) {
        //     state.open = load
        // }
    },
    actions: {
        openCreateModal({ commit }, state) {
            commit('openCreateModal', state)
        },
        createNewAnimal({ commit }, state) {
            commit('createNewAnimal', state)
                // console.log('state', state)
        }
        // openEditModel(context, load) {
        //     context.commit('openEditModel', load)
        // }
    },
    getters: {
        // isModalActive: state => state.open,
        // animalName: state => state.name,
        // animalGender: state => state.gender,
        // animalAge: state => state.age,
        getTodoById: (state) => (id) => {
                return state.todos.find(todo => todo.id === id)
            }
            // get isModalActive() {
            //     return this._isModalActive;
            // },
            // set isModalActive(value) {
            //     this._isModalActive = value;
    }

})